﻿using System.Collections;
using System.Collections.Generic;
using AmphibianSpace;
using UnityEngine;

namespace AmphibianSpace
{
    public class InputController : Singleton<InputController>
    {
        [HideInInspector] public DataInput DataInput;

        private void Awake()
        {
            DataInput = gameObject.AddComponent<DataInput>();
        }

        private void Update()
        {
            if (Input.GetMouseButtonDown(0) && GameController.Instance.DataGame.InputEnable)
            {
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit2D hit = Physics2D.Raycast(ray.origin, ray.direction, Mathf.Infinity);
			    
                if (hit && hit.collider.gameObject.GetComponent<Cell>())
                {
                    DataInput.ClickedCell  = hit.collider.gameObject.GetComponent<Cell>();
                    EventManager.OnClick();
                }
            }
        }
	
    }

}


