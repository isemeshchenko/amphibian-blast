﻿using System.Collections;
using System.Collections.Generic;
using AmphibianSpace;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace AmphibianSpace
{
    public class GameController : Singleton<GameController>
    {
        [HideInInspector]
        public DataColors DataColors;
        [HideInInspector]
        public DataLevel DataLevel;
        [HideInInspector]
        public DataGame DataGame;
        [HideInInspector]
        public DataCached DataCached;
        [HideInInspector]
        public DataPrespawnedObjects DataPrespawnedObjects;

        private void Awake()
        {
            DataLevel = GetComponent<DataLevel>();
            DataColors = GetComponent<DataColors>();
            DataGame = GetComponent<DataGame>();
            DataCached = GetComponent<DataCached>();
            DataPrespawnedObjects = GetComponent<DataPrespawnedObjects>();

            EventManager.GameOver += OnGameOver;
        }


        private IEnumerator Start()
        {
            yield return new WaitForSeconds(1);
            UIController.Instance.UpdateScore();
            UIController.Instance.UpdateTurns();
            
            UIController.Instance.UpdateYellowCells();
            UIController.Instance.UpdateRedCells();
            UIController.Instance.UpdateGreenCells();
            
            PrespawnObjects();
            
            DataCached.Board.FillBoardWithCells();
            DataCached.Board.FillBoardWithTiles();
        }

        void PrespawnObjects()
        {
            foreach (var blocker in DataPrespawnedObjects.BlockersToSpawn)
            {
                DataCached.Board.MakeBlockerAt(blocker.Type,(int) blocker.Position.x,(int) blocker.Position.y);
            }

            for (int i = 0; i < DataPrespawnedObjects.CollectiblesToSpawn.Amount; i++)
            {
                DataCached.Board.MakeCollectibleAt();
            }
           
            foreach (var bomb in DataPrespawnedObjects.BombsToSpawn)
            {
                DataCached.Board.MakeBombAt(bomb.Type,(int) bomb.Position.x,(int) bomb.Position.y);
            }
            
        }

        void OnGameOver()
        {
         //   DataGame.InputEnable = false;

            if (ConditionsCheck())
            {
                UIController.Instance.ShowWinPanel();
                ScoreCheck();
            }
            else
            {
                UIController.Instance.ShowLosePanel();
            }
        }

        bool ConditionsCheck()
        {
            return DataLevel.BlockersLeft <= 0
                   && DataLevel.CollectiblesLeft <= 0
                   && DataLevel.RedCellsLeft <= 0
                   && DataLevel.GreenCellsLeft <= 0
                   && DataLevel.YellowCellsLeft <= 0;
        }

        void ScoreCheck()
        {
            if (DataLevel.CurrentScore < DataLevel.ScoreOneStar)
            {
                UIController.Instance.UpdateStarsText(0);
            }
            if (DataLevel.CurrentScore >= DataLevel.ScoreOneStar && DataLevel.CurrentScore < DataLevel.ScoreTwoStars)
            {
                UIController.Instance.UpdateStarsText(1);
            }
            if (DataLevel.CurrentScore >= DataLevel.ScoreTwoStars && DataLevel.CurrentScore < DataLevel.ScoreThreeStars)
            {
                UIController.Instance.UpdateStarsText(2);
            }
            if (DataLevel.CurrentScore >= DataLevel.ScoreThreeStars)
            {
                UIController.Instance.UpdateStarsText(3);
            }
        }

        public void OnRetry()
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }

        private void OnDestroy()
        {
            EventManager.GameOver -= OnGameOver;
        }
    }

}


