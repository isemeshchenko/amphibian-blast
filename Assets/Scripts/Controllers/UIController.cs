﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Policy;
using AmphibianSpace;
using UnityEngine;

namespace AmphibianSpace
{
    public class UIController : Singleton<UIController>
    {
        [HideInInspector] public DataUI DataUI;

        private void Awake()
        {
            DataUI = GetComponent<DataUI>();
        }

        public void UpdateScore()
        {
            DataUI.CurrentScoreText.text = "Score: " + GameController.Instance.DataLevel.CurrentScore;
        }
        
        public void UpdateTurns()
        {
            DataUI.TurnsLeftText.text = "Turns: " + GameController.Instance.DataLevel.TurnsLeft;
        }

        public void UpdateBlockers()
        {
            if (GameController.Instance.DataLevel.BlockersLeft > 0)
            {
                DataUI.BlockersLeft.text = "Blockers " + GameController.Instance.DataLevel.BlockersLeft;
            }
            else
            {
                DataUI.BlockersLeft.text = "Complete";
            }
        }
        
        public void UpdateCollectibles()
        {
            if (GameController.Instance.DataLevel.CollectiblesLeft > 0)
            {
                DataUI.CollectiblesLeft.text = "Collectibles " + GameController.Instance.DataLevel.CollectiblesLeft;
            }
            else
            {
                DataUI.CollectiblesLeft.text = "Complete";
            }
            
        }
        
        public void UpdateRedCells()
        {
            if (GameController.Instance.DataLevel.RedCellsLeft > 0)
            {
                DataUI.RedCellsLeft.text = "Red Cells " + GameController.Instance.DataLevel.RedCellsLeft;
            }
            else
            {
                DataUI.RedCellsLeft.text = "Complete";
            }
            
        }
        
        public void UpdateGreenCells()
        {
            if (GameController.Instance.DataLevel.GreenCellsLeft > 0)
            {
                DataUI.GreenCellsLeft.text = "Green Cells " + GameController.Instance.DataLevel.GreenCellsLeft;
            }
            else
            {
                DataUI.GreenCellsLeft.text = "Complete";
            }
            
        }
        
        public void UpdateYellowCells()
        {
            if (GameController.Instance.DataLevel.YellowCellsLeft > 0)
            {
                DataUI.YellowCellsLeft.text = "Yellow Cells " + GameController.Instance.DataLevel.YellowCellsLeft;
            }
            else
            {
                DataUI.YellowCellsLeft.text = "Complete";
            }
            
        }

        public void UpdateStarsText(int value)
        {
            DataUI.StarsText.text = "You got " + value + " stars";
        }

        public void ShowWinPanel()
        {
            DataUI.GameOverPanel.SetActive(true);
            DataUI.WinOrLoseText.text = "You Win!";
        }
        
        public void ShowLosePanel()
        {
            DataUI.GameOverPanel.SetActive(true);
            DataUI.WinOrLoseText.text = "You Lose!";
            DataUI.StarsText.text = "";
        }
        
        
    }

}


