﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AmphibianSpace
{
    public static class Tools  
    {
       static List<T> ArrayToList<T>(T[,] array)
        {
            List<T> result = new List<T>();
		
            foreach (var obj in array)
            {
                result.Add(obj);
            }

            return result;
        }
	
    }

}


