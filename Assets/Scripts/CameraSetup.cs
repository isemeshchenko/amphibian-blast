﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AmphibianSpace
{
    public class CameraSetup : MonoBehaviour
    {
        private void Awake()
        {
            SetupCamera(GameController.Instance.DataLevel.BoardWidth, GameController.Instance.DataLevel.BoardHeight);
        }

        public void SetupCamera(int width, int height, int borderSize = 2)
        {
            Camera.main.transform.position = new Vector3((width - 1) / 2f, (height - 1) / 2f, -10f);

            float aspectRatio = (float)Screen.width / Screen.height;

            float verticalSize = height / 2f + borderSize;

            float horizontalSize = (width / 2f + borderSize) / aspectRatio;

            Camera.main.orthographicSize = (verticalSize > horizontalSize) ? verticalSize : horizontalSize;
        }
    }

}


