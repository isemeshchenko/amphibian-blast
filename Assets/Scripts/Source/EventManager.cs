﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AmphibianSpace
{
    public class EventManager : MonoBehaviour
    {
        public delegate void ClickAction();
        public static event ClickAction OnClicked;

        public static void OnClick( )
        {
            if (OnClicked != null)
            {
                OnClicked();
            }
        }
        
        public delegate void GameOverAction();
        public static event ClickAction GameOver;

        public static void OnGameOver( )
        {
            if (GameOver != null)
            {
                GameOver();
            }
        }
        
        

    }

}


