﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Policy;
using UnityEngine;

namespace AmphibianSpace
{
    public abstract class Container : MonoBehaviour 
    {
        protected T Add<T>() where T: Component
        {
            if (gameObject.GetComponent<T>())
            {
                return gameObject.GetComponent<T>();
            }
            
           return gameObject.AddComponent<T>();
        }
        
    }

}


