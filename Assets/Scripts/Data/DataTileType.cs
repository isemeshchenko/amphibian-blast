﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AmphibianSpace
{
    public enum TileTypeEnum
    {
        Empty,
        Normal,
        None
    }
    
    [System.Serializable]
    public class DataTileType : MonoBehaviour
    {
        public TileTypeEnum TileType;
    }

}


