﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AmphibianSpace
{
    [System.Serializable]
    public class DataPosition : MonoBehaviour
    {
	    public int X;
        public int Y;

    }

}


