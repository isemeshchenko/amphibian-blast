﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AmphibianSpace
{
    
    public class DataPrespawnedObjects : MonoBehaviour 
    {
        public BlockerInfo[] BlockersToSpawn;
        public CollectibleInfo CollectiblesToSpawn;
        public BombInfo[] BombsToSpawn;
        
        
        [System.Serializable]
        public struct BlockerInfo
        {
            public BlockerTypeEnum Type;
            public Vector2 Position;
        }
        
        
        [System.Serializable]
        public struct CollectibleInfo
        {
            public int Amount;
        }
        
        [System.Serializable]
        public struct BombInfo
        {
            public BombTypeEnum Type;
            public Vector2 Position;
        }
        
        
    }

}


