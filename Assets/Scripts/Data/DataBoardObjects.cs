﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AmphibianSpace
{
	public class DataBoardObjects : MonoBehaviour
	{
		public Tile[,] AllTiles;
		public BoardObject[,] AllBoardObjects;
		public Cell ClickedCell;
	}

}


