﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AmphibianSpace
{
    public enum BombTypeEnum
    {
        Row = 0,
        Column,
        Adjacent,
        Tsunami,
        Color,
        None
    }
    
    [System.Serializable]
    public class DataBombType : MonoBehaviour
    {
        public BombTypeEnum BombType;

    }

}


