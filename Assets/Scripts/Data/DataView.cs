﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AmphibianSpace
{
    public class DataView : MonoBehaviour
    {
        [HideInInspector]
        public SpriteRenderer SpriteRenderer;

        private void Awake()
        {
            SpriteRenderer = GetComponentInChildren<SpriteRenderer>();
        }
    }

}


