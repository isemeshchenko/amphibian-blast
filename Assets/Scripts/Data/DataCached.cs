﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AmphibianSpace
{
    [System.Serializable]
    public class DataCached : MonoBehaviour
    {
        [HideInInspector]
        public Board Board;

        private void Awake()
        {
            Board = FindObjectOfType<Board>();
        }
    }

}


