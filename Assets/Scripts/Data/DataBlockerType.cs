﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AmphibianSpace
{
    public enum BlockerTypeEnum
    {
        Blocker1HP,
        None
    }
    
    [System.Serializable]
    public class DataBlockerType : MonoBehaviour
    {
        public BlockerTypeEnum BlockerType;

    }

}


