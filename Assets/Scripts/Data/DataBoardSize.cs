﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AmphibianSpace
{
    [System.Serializable]
    public class DataBoardSize : MonoBehaviour
    {
        public int Width =9;
        public int Height=9;

    }

}


