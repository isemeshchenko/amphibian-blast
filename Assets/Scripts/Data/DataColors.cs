﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AmphibianSpace
{
    [System.Serializable]
    public class DataColors : MonoBehaviour
    {
        public Color Yellow;
        public Color Red;
        public Color Orange;
        public Color Blue;
        public Color Purple;
        public Color Green;
        public Color TileDefault;
        public Color Bomb;
        
        [HideInInspector]
        public Dictionary<MatchValueEnum, Color> MatchColorDict = new Dictionary<MatchValueEnum, Color>();

        private void Awake()
        {
            MatchColorDict.Add(MatchValueEnum.Yellow, Yellow);
            MatchColorDict.Add(MatchValueEnum.Red, Red);
            MatchColorDict.Add(MatchValueEnum.Orange, Orange);
            MatchColorDict.Add(MatchValueEnum.Blue, Blue);
            MatchColorDict.Add(MatchValueEnum.Purple, Purple);
            MatchColorDict.Add(MatchValueEnum.Green, Green);
            MatchColorDict.Add(MatchValueEnum.Bomb, Bomb);
            
        }
    }

}


