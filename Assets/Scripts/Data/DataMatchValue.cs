﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AmphibianSpace
{
    public enum MatchValueEnum
    {
        Yellow = 0,
        Red,
        Orange,
        Blue,
        Purple,
        Green,
        Bomb,
        None
        
    }
    [System.Serializable]
    public class DataMatchValue : MonoBehaviour
    {
        public MatchValueEnum MatchValue;

    }

}


