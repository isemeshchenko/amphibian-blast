﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace AmphibianSpace
{
    [System.Serializable]
    public class DataUI : MonoBehaviour
    {
        public TextMeshProUGUI CurrentScoreText;
        public TextMeshProUGUI TurnsLeftText;
        public TextMeshProUGUI BlockersLeft;
        public TextMeshProUGUI CollectiblesLeft;
        public TextMeshProUGUI RedCellsLeft;
        public TextMeshProUGUI GreenCellsLeft;
        public TextMeshProUGUI YellowCellsLeft;

        public TextMeshProUGUI WinOrLoseText;
        public TextMeshProUGUI StarsText;

        public GameObject GameOverPanel;

    }

}


