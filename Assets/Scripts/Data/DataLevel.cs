﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AmphibianSpace
{
    [System.Serializable]
    public class DataLevel : MonoBehaviour
    {
        public int BoardWidth;
        public int BoardHeight;
        
        public int TurnsLeft;

//        public int TimeToComplete;
//        public int CurrentTime;
        
        public int CurrentScore;
        [HideInInspector]
        public int BlockersLeft;
        [HideInInspector]
        public int CollectiblesLeft;

        public int RedCellsLeft;
        public int GreenCellsLeft;
        public int YellowCellsLeft;
        
        [HideInInspector]
        public int BlueCellsLeft;
        [HideInInspector]
        public int OrangeCellsLeft;
        [HideInInspector]
        public int PurpleCellsLeft;
        
        public int ScoreOneStar;
        public int ScoreTwoStars;
        public int ScoreThreeStars;
        
       
        
        
    }


}

