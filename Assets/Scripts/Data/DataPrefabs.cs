﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AmphibianSpace
{
    [System.Serializable]
    public class DataPrefabs : MonoBehaviour
    {
        public GameObject YellowCell;
        public GameObject RedCell;
        public GameObject OrangeCell;
        public GameObject BlueCell;
        public GameObject PurpleCell;
        public GameObject GreenCell;

        public GameObject TileEmpty;
        public GameObject TileNormal;

        public GameObject BombRow;
        public GameObject BombColumn;
        public GameObject BombAdjacent;
        public GameObject BombTsunami;
        public GameObject BombColor;

        public GameObject Blocker1HP;

        public GameObject Collectible1;
        public GameObject Collectible2;
        
        public Dictionary<MatchValueEnum, GameObject> CellPrefabDict = new Dictionary<MatchValueEnum, GameObject>();
        public Dictionary<BombTypeEnum, GameObject> BombPrefabDict = new Dictionary<BombTypeEnum, GameObject>();
        public Dictionary<TileTypeEnum, GameObject> TilePrefabDict = new Dictionary<TileTypeEnum, GameObject>();
        public Dictionary<BlockerTypeEnum, GameObject> BlockerPrefabDict = new Dictionary<BlockerTypeEnum, GameObject>();
        
        [HideInInspector]
        public List<GameObject> Collectibles = new List<GameObject>();

        private void Awake()
        {
            CellPrefabDict.Add(MatchValueEnum.Yellow, YellowCell);
            CellPrefabDict.Add(MatchValueEnum.Red, RedCell);
            CellPrefabDict.Add(MatchValueEnum.Orange, OrangeCell);
            CellPrefabDict.Add(MatchValueEnum.Blue, BlueCell);
            CellPrefabDict.Add(MatchValueEnum.Purple, PurpleCell);
            CellPrefabDict.Add(MatchValueEnum.Green, GreenCell);
            
            BombPrefabDict.Add(BombTypeEnum.Row, BombRow);
            BombPrefabDict.Add(BombTypeEnum.Column, BombColumn);
            BombPrefabDict.Add(BombTypeEnum.Adjacent, BombAdjacent);
            BombPrefabDict.Add(BombTypeEnum.Tsunami, BombTsunami);
            BombPrefabDict.Add(BombTypeEnum.Color, BombColor);
            
            TilePrefabDict.Add(TileTypeEnum.Empty, TileEmpty);
            TilePrefabDict.Add(TileTypeEnum.Normal, TileNormal);
            
            BlockerPrefabDict.Add(BlockerTypeEnum.Blocker1HP, Blocker1HP);
            
            Collectibles.Add(Collectible1);
            Collectibles.Add(Collectible2);
            
            
            
        }
    }

}


