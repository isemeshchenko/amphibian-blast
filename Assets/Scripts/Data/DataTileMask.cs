﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AmphibianSpace
{
    public enum TileMaskEnum
    {
        None,
        RowColumn,
        Adjacent,
        Tsunami
    }
    
    [System.Serializable]
    public class DataTileMask : MonoBehaviour
    {
        public Sprite RowColumnMask;
        public Sprite AdjacentMask;
        public Sprite TsunamiMask;
        
        [HideInInspector]
        public SpriteRenderer MaskRenderer;
        [HideInInspector]
        public GameObject MaskObject;
        [HideInInspector]
        public TileMaskEnum TileMask;
        
        
        public Dictionary<TileMaskEnum, Sprite> TileMaskDict = new Dictionary<TileMaskEnum, Sprite>();
        
        

        private void Awake()
        {
            TileMaskDict.Add(TileMaskEnum.RowColumn, RowColumnMask);
            TileMaskDict.Add(TileMaskEnum.Adjacent, AdjacentMask);
            TileMaskDict.Add(TileMaskEnum.Tsunami, TsunamiMask);

            MaskObject = GameObject.Find("TileNormal/view/mask");

            if (MaskObject != null)
            {
                MaskRenderer = MaskObject.GetComponent<SpriteRenderer>();
            }
        }
    }

}

