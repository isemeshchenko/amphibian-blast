﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AmphibianSpace
{
    [System.Serializable]
    public class DataGame : MonoBehaviour
    {
        [HideInInspector]
        public int MinCellMatchCount = 2;
        [HideInInspector]
        public int MinBombMatchCount = 1;

        public float RoutineTime = 1;
        public float ClearTime = 0.1f;
        
        public bool InputEnable = true;

        [HideInInspector] public int RowColumnBombMinMatch = 5;
        [HideInInspector] public int AdjacentBombMinMatch = 7;
        [HideInInspector] public int TsunamiBombMinMatch = 9;
    }

}


