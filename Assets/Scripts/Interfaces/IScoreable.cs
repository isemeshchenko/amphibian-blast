﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AmphibianSpace
{
    public interface IScoreable
    {
        int GetScore();

    }
}


