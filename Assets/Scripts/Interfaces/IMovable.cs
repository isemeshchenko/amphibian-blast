﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.WSA;

namespace AmphibianSpace
{
    public interface IMovable
    {
        void Move(int x, int y);

    }

}


