﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AmphibianSpace
{
	public class AdjacentBomb : Bomb 
	{
		private void Awake()
		{
			Setup();
			DataBombType.BombType = BombTypeEnum.Adjacent;
		}
		
		public override List<BoardObject> GetCellsToClear()
		{
			return DataCached.Board.FindAdjacentMatches(this);
		}

		
	
	}

}
