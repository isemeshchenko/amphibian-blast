﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AmphibianSpace
{
	public class TsunamiBomb : Bomb
	{
		private void Awake()
		{
			Setup();
			DataBombType.BombType = BombTypeEnum.Tsunami;
		}
		
		public override List<BoardObject> GetCellsToClear()
		{
			return DataCached.Board.FindTsunamiMatches(this);
		}

		
	
	}

}
