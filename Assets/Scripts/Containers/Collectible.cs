﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AmphibianSpace
{
    public class Collectible : BoardObject, IMovable
    {
        private void Awake()
        {
            Setup();
        }
        
        public  void Init()
        {
            DataCached.Board.DataBoardObjects.AllBoardObjects[DataPosition.X, DataPosition.Y] = this;
            
            GameController.Instance.DataLevel.CollectiblesLeft++;
            UIController.Instance.UpdateCollectibles();
        }

        private void OnClear()
        {
            DataCached.Board.DataBoardObjects.AllBoardObjects[DataPosition.X, DataPosition.Y] = null;
            Destroy(gameObject);
            
            GameController.Instance.DataLevel.CurrentScore += GetScore();
            UIController.Instance.UpdateScore();
            
            GameController.Instance.DataLevel.CollectiblesLeft--;
            UIController.Instance.UpdateCollectibles();
        }

        public void Move(int x, int y)
        {
            SetCoordinates(x,y);
            if (y == 0)
            {
                OnClear();
            }
        }
    }

}


