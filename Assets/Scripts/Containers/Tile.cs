﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AmphibianSpace
{
    public class Tile : BoardObject
    {
        [HideInInspector]
        public DataTileType DataTileType;
        
        [HideInInspector]
        public DataTileMask DataTileMask;


        private void Awake()
        {
            Setup();
        }

        protected override void Setup()
        {
            base.Setup();
            DataTileType = Add<DataTileType>();
            DataTileMask = Add<DataTileMask>();
        }

        public void Init()
        {
            DataCached.Board.DataBoardObjects.AllTiles[DataPosition.X, DataPosition.Y] = this;
        }
        
        public void ChangeColor(Color color)
        {
            DataView.SpriteRenderer.color = color;
        }

        public void ShowMask(bool value)
        {
            DataTileMask.MaskObject.SetActive(value);
        }

        public void ChangeMask(TileMaskEnum tileMask)
        {
            DataTileMask.MaskRenderer.sprite = DataTileMask.TileMaskDict[tileMask];
        }
    }

}


