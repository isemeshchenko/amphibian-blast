﻿using System.Collections;
using System.Collections.Generic;
using AmphibianSpace;
using UnityEngine;

namespace AmphibianSpace
{
    public class Blocker : BoardObject, IClearable
    {
        [HideInInspector]
        public DataHealth DataHealth;

        private void Awake()
        {
            Setup();
        }

        protected override void Setup()
        {
            base.Setup();
            DataHealth = Add<DataHealth>();
        }
        
        public  void Init()
        {
            DataCached.Board.DataBoardObjects.AllBoardObjects[DataPosition.X, DataPosition.Y] = this;
            
            GameController.Instance.DataLevel.BlockersLeft++;
            UIController.Instance.UpdateBlockers();
        }
        
        public void OnClear()
        {
            DataHealth.Health--;
            if (DataHealth.Health == 0)
            {
                Destroy(gameObject);
                
                GameController.Instance.DataLevel.CurrentScore += GetScore();
                UIController.Instance.UpdateScore();

                GameController.Instance.DataLevel.BlockersLeft--;
                UIController.Instance.UpdateBlockers();
            }
        }
        
        
    }

}


