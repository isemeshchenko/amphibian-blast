﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AmphibianSpace
{
    public class TileNormal : Tile 
    {
        [HideInInspector]
        public DataTileMask DataTileMask;

        private void Awake()
        {
            Setup();
            DataTileType.TileType = TileTypeEnum.Normal;
        }

        protected override void Setup()
        {
            base.Setup();
            DataTileMask = Add<DataTileMask>();
        }
        
        public void ChangeColor(Color color)
        {
            DataView.SpriteRenderer.color = color;
        }

        public void ShowMask(bool value)
        {
            DataTileMask.MaskObject.SetActive(value);
        }

        public void ChangeMask(TileMaskEnum tileMask)
        {
            DataTileMask.MaskRenderer.sprite = DataTileMask.TileMaskDict[tileMask];
        }
        
        
    }
    
    

}


