﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AmphibianSpace
{
    public class BoardObject : Container, IScoreable
    {
        [HideInInspector]
        public DataPosition DataPosition;
        [HideInInspector]
        public DataCached DataCached;
        [HideInInspector]
        public DataView DataView;
        [HideInInspector]
        public DataScore DataScore;
        [HideInInspector]
        public DataName DataName;
       

        public void SetCoordinates(int x, int y, int z = 0)
        {
            DataPosition.X = x;
            DataPosition.Y = y;
            
            transform.position = new Vector3(x,y);
            Rename(x,y);
        }

        protected virtual void Setup()
        {
            DataPosition = Add<DataPosition>();
            DataCached = Add<DataCached>();
            DataView = Add<DataView>();
            DataScore = Add<DataScore>();
            DataName = Add<DataName>();
        }

        private void Rename( int x, int y)
        {
            gameObject.name = DataName.Name + " (" + x + "," + y + ")";
        }

        public int GetScore()
        {
            return DataScore.Score;
        }
    }

}


