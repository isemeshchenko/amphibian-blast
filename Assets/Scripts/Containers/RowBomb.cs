﻿using System.Collections;
using System.Collections.Generic;
using AmphibianSpace;
using UnityEngine;

namespace AmphibianSpace
{
    public class RowBomb : Bomb
    {
        protected override void Awake()
        {
            Setup();
            DataBombType.BombType = BombTypeEnum.Row;
        }

        public override List<BoardObject> GetCellsToClear()
        {
            return DataCached.Board.FindRowMatches(this);
        }
        
    }

}


