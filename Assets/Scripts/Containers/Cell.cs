﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;

namespace AmphibianSpace
{
    public class Cell : BoardObject, IClearable, IMovable
    {
        [HideInInspector]
        public DataMatchValue DataMatchValue;

        protected virtual void Awake()
        {
            Setup();
        }

        protected override void Setup()
        {
            base.Setup();
            DataMatchValue = Add<DataMatchValue>();
        }

        public virtual void Init()
        {
            DataCached.Board.DataBoardObjects.AllBoardObjects[DataPosition.X, DataPosition.Y] = this;
        }
        
        public virtual void OnClear()
        {
           DataCached.Board.DataBoardObjects.AllBoardObjects[DataPosition.X, DataPosition.Y] = null;

           GameController.Instance.DataLevel.CurrentScore += GetScore();
           UIController.Instance.UpdateScore();

            switch (DataMatchValue.MatchValue)
            {
                case MatchValueEnum.Blue:
                {
                    GameController.Instance.DataLevel.BlueCellsLeft--;
                    break;
                }
                case MatchValueEnum.Yellow:
                {
                    GameController.Instance.DataLevel.YellowCellsLeft--;
                    UIController.Instance.UpdateYellowCells();
                    break;
                }
                case MatchValueEnum.Red:
                {
                    GameController.Instance.DataLevel.RedCellsLeft--;
                    UIController.Instance.UpdateRedCells();
                    break;
                }
                case MatchValueEnum.Orange:
                {
                    GameController.Instance.DataLevel.OrangeCellsLeft--;
                    break;
                }
                case MatchValueEnum.Green:
                {
                    GameController.Instance.DataLevel.GreenCellsLeft--;
                    UIController.Instance.UpdateGreenCells();
                    break;
                }
                case MatchValueEnum.Purple:
                {
                    GameController.Instance.DataLevel.PurpleCellsLeft--;
                    break;
                }
            }
           
           Destroy(gameObject);
        }


        public void Move(int x, int y)
        {
            SetCoordinates(x,y);
        }
    }

}


