﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AmphibianSpace
{
    public abstract class  Bomb : Cell
    {
        [HideInInspector]
        public DataBombType DataBombType;

        protected override void Setup()
        {
            base.Setup();
            DataBombType = Add<DataBombType>();
            DataMatchValue.MatchValue = MatchValueEnum.Bomb;
        }

        public override void Init()
        {
            DataCached.Board.DataBoardObjects.AllBoardObjects[DataPosition.X, DataPosition.Y] = this;
        }

        public override void OnClear()
        {
            DataCached.Board.DataBoardObjects.AllBoardObjects[DataPosition.X, DataPosition.Y] = null;
            Destroy(gameObject);
            
            GameController.Instance.DataLevel.CurrentScore += GetScore();
            UIController.Instance.UpdateScore();
        }

        public abstract List<BoardObject> GetCellsToClear();
    }
    
    

}


