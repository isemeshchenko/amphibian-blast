﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AmphibianSpace
{
	public class TileEmpty : Tile 
	{
		private void Awake()
		{
			base.Setup();
			DataTileType.TileType = TileTypeEnum.Empty;
		}
	}

}


