﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using UnityEngine;

namespace AmphibianSpace
{
    public class Board : Container
    {
        [HideInInspector] public DataBoardObjects DataBoardObjects;
        [HideInInspector] public DataBoardSize DataBoardSize;

        private void Awake()
        {
            DataBoardObjects = Add<DataBoardObjects>();
            DataBoardSize = Add<DataBoardSize>();
        }

        private void Start()
        {
            DataBoardSize.Width = GameController.Instance.DataLevel.BoardWidth;
            DataBoardSize.Height = GameController.Instance.DataLevel.BoardHeight;

            DataBoardObjects.AllTiles = new Tile[DataBoardSize.Width, DataBoardSize.Height];
            DataBoardObjects.AllBoardObjects = new BoardObject[DataBoardSize.Width, DataBoardSize.Height];

            EventManager.OnClicked += OnClick;
        }

        private void OnClick()
        {
            if (InputController.Instance.DataInput.ClickedCell != null)
            {
                DataBoardObjects.ClickedCell = InputController.Instance.DataInput.ClickedCell;

                if (DataBoardObjects.ClickedCell.DataMatchValue.MatchValue != MatchValueEnum.Bomb)
                {
                    ClearAndCollapse(FindAllMatches(DataBoardObjects.ClickedCell));
                }
                else
                {
                    Bomb clickedBomb = DataBoardObjects.ClickedCell as Bomb;

                    if (clickedBomb != null)
                    {
                        ClearAndCollapse(FindAllBombsMatches(FindAllMatches(DataBoardObjects.ClickedCell,
                            GameController.Instance.DataGame.MinBombMatchCount)));
                    }
                }

                GameController.Instance.DataLevel.TurnsLeft--;
                UIController.Instance.UpdateTurns();

                if (GameController.Instance.DataLevel.TurnsLeft == 0)
                {
                    EventManager.OnGameOver();
                }
            }
        }


        #region FillBoard

        public void FillBoardWithCells()
        {
            for (int i = 0; i < DataBoardSize.Width; i++)
            {
                for (int j = 0; j < DataBoardSize.Height; j++)
                {
                    if (DataBoardObjects.AllBoardObjects[i, j] == null)
                    {
                        MakeCellAt(i, j);
                    }
                }
            }
        }

        public void FillBoardWithTiles()
        {
            for (int i = 0; i < DataBoardSize.Width; i++)
            {
                for (int j = 0; j < DataBoardSize.Height; j++)
                {
                    if (DataBoardObjects.AllTiles[i, j] == null)
                    {
                        MakeTileAt(TileTypeEnum.Normal, i, j);
                    }
                }
            }
        }

        #endregion

        #region MakeObjects

        void MakeTileAt(TileTypeEnum tileType, int x, int y)
        {
            if (IsWithinBounds(x, y))
            {
                GameObject obj = Factory.Instance.GetTile(tileType);

                Tile tile = obj.GetComponent<Tile>();
                tile.transform.parent = transform;
                tile.SetCoordinates(x, y);
                tile.Init();
            }
        }


        void MakeCellAt(MatchValueEnum matchValue, int x, int y)
        {
            if (IsWithinBounds(x, y))
            {
                GameObject obj = Factory.Instance.GetCell(matchValue);

                Cell cell = obj.GetComponent<Cell>();
                cell.transform.parent = transform;
                cell.SetCoordinates(x, y);
                cell.Init();
            }
        }

        void MakeCellAt(int x, int y)
        {
            if (IsWithinBounds(x, y) && DataBoardObjects.AllBoardObjects[x, y] == null)
            {
                GameObject obj = Factory.Instance.GetRandomCell();

                Cell cell = obj.GetComponent<Cell>();
                cell.transform.parent = transform;
                cell.SetCoordinates(x, y);
                cell.Init();
            }
        }

        public void MakeBombAt(BombTypeEnum bombType, int x, int y)
        {
            if (IsWithinBounds(x, y) && bombType != BombTypeEnum.None && DataBoardObjects.AllBoardObjects[x, y] == null)
            {
                GameObject obj = Factory.Instance.GetBomb(bombType);

                Bomb bomb = obj.GetComponent<Bomb>();
                bomb.transform.parent = transform;
                bomb.SetCoordinates(x, y);
                bomb.Init();
            }
        }

        public void MakeBlockerAt(BlockerTypeEnum blockerType, int x, int y)
        {
            if (IsWithinBounds(x, y) && blockerType != BlockerTypeEnum.None &&
                DataBoardObjects.AllBoardObjects[x, y] == null)
            {
                GameObject obj = Factory.Instance.GetBlocker(blockerType);

                Blocker blocker = obj.GetComponent<Blocker>();
                blocker.transform.parent = transform;
                blocker.SetCoordinates(x, y);
                blocker.Init();
            }
        }

        public void MakeCollectibleAt(int x, int y)
        {
            if (IsWithinBounds(x, y))
            {
                GameObject obj = Factory.Instance.GetCollectible();

                Collectible collectible = obj.GetComponent<Collectible>();
                collectible.transform.parent = transform;
                collectible.SetCoordinates(x, y);
                collectible.Init();
            }
        }

        public void MakeCollectibleAt()
        {
            int random = Random.Range(0, DataBoardSize.Width);

            if (DataBoardObjects.AllBoardObjects[random, DataBoardSize.Height - 1] is Collectible)
            {
                MakeCollectibleAt();
            }
            else
            {
                GameObject obj = Factory.Instance.GetCollectible();
                Collectible collectible = obj.GetComponent<Collectible>();
                collectible.transform.parent = transform;
                collectible.SetCoordinates(random, DataBoardSize.Height - 1);
                collectible.Init();
            }
        }

        #endregion

        #region FindMatchesCells

        private List<Cell> FindAllNextMatches(Cell cell)
        {
            List<Cell> horizontalMatches = FindHorizontalMatches(cell);
            List<Cell> verticalMatches = FindVerticalMatches(cell);

            return horizontalMatches.Union(verticalMatches).ToList();
        }

        List<Cell> FindAllNextMatches(List<Cell> cells)
        {
            List<Cell> allMatches = new List<Cell>();

            foreach (var cell in cells)
            {
                List<Cell> matches = FindAllNextMatches(cell);
                allMatches = allMatches.Union(matches).ToList();
            }

            return allMatches;
        }

        private List<Cell> FindAllMatches(List<Cell> cells, int minMatchNumber = 2)
        {
            List<Cell> startMatches = cells;
            List<Cell> newMatches = FindAllNextMatches(cells);

            while (newMatches.Count > startMatches.Count)
            {
                startMatches = newMatches;
                newMatches = FindAllNextMatches(newMatches);
            }

            return newMatches.Count >= minMatchNumber ? newMatches : null;
        }

        private List<Cell> FindAllMatches(Cell cell, int minMatchNumber = 2)
        {
            List<Cell> startMatches = FindAllNextMatches(cell);
            List<Cell> newMatches = FindAllNextMatches(startMatches);

            while (newMatches.Count > startMatches.Count)
            {
                startMatches = newMatches;
                newMatches = FindAllMatches(newMatches, minMatchNumber);
            }

            return newMatches.Count >= minMatchNumber ? newMatches : null;
        }

        List<Cell> FindHorizontalMatches(Cell startCell)
        {
            List<Cell> matches = new List<Cell>();

            if (startCell != null)
            {
                for (int i = startCell.DataPosition.X - 1; i <= startCell.DataPosition.X + 1; i++)
                {
                    if (IsWithinBounds(i, startCell.DataPosition.Y) &&
                        DataBoardObjects.AllBoardObjects[i, startCell.DataPosition.Y] is Cell)
                    {
                        Cell nextHorCell = (Cell) DataBoardObjects.AllBoardObjects[i, startCell.DataPosition.Y];

                        if (nextHorCell != null &&
                            nextHorCell.DataMatchValue.MatchValue == startCell.DataMatchValue.MatchValue)
                        {
                            matches.Add(nextHorCell);
                        }
                    }
                }
            }
            return matches;
        }


        List<Cell> FindVerticalMatches(Cell startCell)
        {
            List<Cell> matches = new List<Cell>();

            if (startCell != null)
            {
                for (int j = startCell.DataPosition.Y - 1; j <= startCell.DataPosition.Y + 1; j++)
                {
                    if (IsWithinBounds(startCell.DataPosition.X, j) &&
                        DataBoardObjects.AllBoardObjects[startCell.DataPosition.X, j] is Cell)
                    {
                        Cell nextVertCell = (Cell) DataBoardObjects.AllBoardObjects[startCell.DataPosition.X, j];

                        if (nextVertCell != null &&
                            nextVertCell.DataMatchValue.MatchValue == startCell.DataMatchValue.MatchValue)
                        {
                            matches.Add(nextVertCell);
                        }
                    }
                }
            }

            return matches;
        }

        #endregion

        #region FindMatchesBombs

        private List<BoardObject> FindAllBombsMatches(List<Cell> bombs)
        {
            List<BoardObject> result = new List<BoardObject>();

            foreach (var item in bombs)
            {
                Bomb bomb = item as Bomb;

                if (bomb != null)
                {
                    result = result.Union(bomb.GetCellsToClear()).ToList();
                }
            }

            return result;
        }

        public List<BoardObject> FindRowMatches(Bomb bomb)
        {
            List<BoardObject> matches = new List<BoardObject>();

            for (int i = bomb.DataPosition.X - 2; i <= bomb.DataPosition.X + 2; i++)
            {
                if (IsWithinBounds(i, bomb.DataPosition.Y) && DataBoardObjects.AllBoardObjects[i, bomb.DataPosition.Y])
                {
                    BoardObject boardObj = DataBoardObjects.AllBoardObjects[i, bomb.DataPosition.Y];

                    if (boardObj is IClearable)
                    {
                        matches.Add(DataBoardObjects.AllBoardObjects[i, bomb.DataPosition.Y]);
                    }
                }
            }
            return matches;
        }

        public List<BoardObject> FindColumnMatches(Bomb bomb)
        {
            List<BoardObject> matches = new List<BoardObject>();

            for (int i = bomb.DataPosition.Y - 2; i <= bomb.DataPosition.Y + 2; i++)
            {
                if (IsWithinBounds(bomb.DataPosition.X, i))
                {
                    BoardObject boardObj = DataBoardObjects.AllBoardObjects[bomb.DataPosition.X, i];

                    if (boardObj is IClearable)
                    {
                        matches.Add(DataBoardObjects.AllBoardObjects[bomb.DataPosition.X, i]);
                    }
                }
            }
            return matches;
        }

        public List<BoardObject> FindAdjacentMatches(Bomb bomb)
        {
            List<BoardObject> matches = new List<BoardObject>();

            for (int i = bomb.DataPosition.X - 1; i <= bomb.DataPosition.X + 1; i++)
            {
                for (int j = bomb.DataPosition.Y - 1; j <= bomb.DataPosition.Y + 1; j++)
                {
                    if (IsWithinBounds(i, j))
                    {
                        BoardObject boardObj = DataBoardObjects.AllBoardObjects[i, j];


                        if (boardObj is IClearable)
                        {
                            matches.Add(DataBoardObjects.AllBoardObjects[i, j]);
                        }
                    }
                }
            }

            return matches;
        }

        public List<BoardObject> FindTsunamiMatches(Bomb bomb)
        {
            List<BoardObject> matches = new List<BoardObject>();

            for (int i = bomb.DataPosition.X - 2; i <= bomb.DataPosition.X + 2; i++)
            {
                for (int j = bomb.DataPosition.Y - 2; j <= bomb.DataPosition.Y + 2; j++)
                {
                    if (IsWithinBounds(i, j))
                    {
                        BoardObject boardObj = DataBoardObjects.AllBoardObjects[i, j];

                        if (boardObj is IClearable)
                        {
                            matches.Add(DataBoardObjects.AllBoardObjects[i, j]);
                        }
                    }
                }
            }

            return matches;
        }

        #endregion

        #region Highlight

        private void HighlightMatchesOn(List<Cell> matches)
        {
            if (matches != null)
            {
                foreach (var cell in matches)
                {
                    Tile tile = DataBoardObjects.AllTiles[cell.DataPosition.X, cell.DataPosition.Y];

                    tile.ChangeColor(GameController.Instance.DataColors.MatchColorDict[cell.DataMatchValue.MatchValue]);
                }
            }
        }

        private void HighlightMatchesOn(List<Cell> matches, Color color)
        {
            if (matches != null)
            {
                foreach (var cell in matches)
                {
                    Tile tile = DataBoardObjects.AllTiles[cell.DataPosition.X, cell.DataPosition.Y];

                    tile.ChangeColor(color);
                }
            }
        }

        private void HighlightMatchesOn(List<BoardObject> matches, Color color)
        {
            if (matches != null)
            {
                foreach (var cell in matches)
                {
                    Tile tile = DataBoardObjects.AllTiles[cell.DataPosition.X, cell.DataPosition.Y];

                    tile.ChangeColor(color);
                }
            }
        }

        private void HighlightMatchesOff(List<Cell> matches)
        {
            if (matches != null)
            {
                foreach (var cell in matches)
                {
                    Tile tile = DataBoardObjects.AllTiles[cell.DataPosition.X, cell.DataPosition.Y];

                    tile.ChangeColor(GameController.Instance.DataColors.TileDefault);
                }
            }
        }

        #endregion

        #region ClearObjects

        private void ClearCellAt(int x, int y)
        {
            Cell cellToClear = (Cell) DataBoardObjects.AllBoardObjects[x, y];

            if (cellToClear != null && IsWithinBounds(x, y))
            {
                cellToClear.OnClear();

                ClearBlocker(x, y);
            }
        }

        private void ClearBlocker(int x, int y)
        {
            if (FindNextBlockers(x, y) != null)
            {
                foreach (var blocker in FindNextBlockers(x, y))
                {
                    blocker.OnClear();
                }
            }
        }

        #endregion

        #region CollapseObjects

        private void CollapseColumn(int column)
        {
            for (int i = 0; i < DataBoardSize.Height - 1; i++)
            {
                if (DataBoardObjects.AllBoardObjects[column, i] == null)
                {
                    for (int j = i + 1; j < DataBoardSize.Height; j++)
                    {
                        IMovable movableObj = DataBoardObjects.AllBoardObjects[column, j] as IMovable;

                        if (movableObj != null)
                        {
                            movableObj.Move(column, i);

                            DataBoardObjects.AllBoardObjects[column, i] = DataBoardObjects.AllBoardObjects[column, j];
                            DataBoardObjects.AllBoardObjects[column, j] = null;

                            break;
                        }
                    }
                }
            }
        }

        private void CollapseColumn(List<int> columns)
        {
            if (columns != null)
            {
                foreach (var column in columns)
                {
                    CollapseColumn(column);
                }
            }
        }


        List<int> GetColumns(List<BoardObject> boardObjects)
        {
            if (boardObjects != null)
            {
                List<int> columns = new List<int>();

                foreach (BoardObject boardObj in boardObjects)
                {
                    if (boardObj != null)
                    {
                        if (!columns.Contains(boardObj.DataPosition.X))
                        {
                            columns.Add(boardObj.DataPosition.X);
                        }
                    }
                }

                return columns;
            }
            return null;
        }

        List<int> GetColumns(List<Cell> cells)
        {
            if (cells != null)
            {
                List<int> columns = new List<int>();

                foreach (Cell cell in cells)
                {
                    if (cell != null)
                    {
                        if (!columns.Contains(cell.DataPosition.X))
                        {
                            columns.Add(cell.DataPosition.X);
                        }
                    }
                }

                return columns;
            }
            return null;
        }

        #endregion

        #region Tools

        bool IsWithinBounds(int x, int y)
        {
            return (x >= 0 && x < DataBoardSize.Width && y >= 0 && y < DataBoardSize.Height);
        }

        void ResetTiles()
        {
            foreach (var tile in DataBoardObjects.AllTiles)
            {
                tile.ChangeColor(GameController.Instance.DataColors.TileDefault);
            }
        }

        BombTypeEnum ChooseBomb(List<Cell> cells)
        {
            if (cells != null && cells.Count >= GameController.Instance.DataGame.RowColumnBombMinMatch)
            {
                if (cells.Count < GameController.Instance.DataGame.AdjacentBombMinMatch)
                {
                    int randomIndex = Random.Range(0, 2);
                    return (BombTypeEnum) randomIndex;
                }
                if (cells.Count >= GameController.Instance.DataGame.AdjacentBombMinMatch
                    && cells.Count < GameController.Instance.DataGame.TsunamiBombMinMatch)
                {
                    return BombTypeEnum.Adjacent;
                }
                if (cells.Count >= GameController.Instance.DataGame.TsunamiBombMinMatch)
                {
                    return BombTypeEnum.Tsunami;
                }
            }

            return BombTypeEnum.None;
        }


        void ShowPossibleMatches(BoardObject[,] boardObjects)
        {
            foreach (var boardObj in boardObjects)
            {
                Cell cell = boardObj as Cell;

                if (cell != null)
                {
                    List<Cell> matches = FindAllMatches(cell);

                    if (matches != null)
                    {
                        HighlightMatchesOn(matches,
                            GameController.Instance.DataColors.MatchColorDict[cell.DataMatchValue.MatchValue]);
                    }
                }
            }
        }

        List<BoardObject> FindNextBoardObjects(int x, int y)
        {
            List<BoardObject> result = new List<BoardObject>();

            for (int i = x - 1; i <= x + 1; i++)
            {
                if (IsWithinBounds(i, y))
                {
                    result.Add(DataBoardObjects.AllBoardObjects[i, y]);
                }
            }
            for (int j = y - 1; j <= y + 1; j++)
            {
                if (IsWithinBounds(x, j))
                {
                    result.Add(DataBoardObjects.AllBoardObjects[x, j]);
                }
            }
            return result;
        }

        List<Blocker> FindNextBlockers(int x, int y)
        {
            List<Blocker> result = new List<Blocker>();

            foreach (var obj in FindNextBoardObjects(x, y))
            {
                var blocker = obj as Blocker;
                if (blocker != null)
                {
                    result.Add(blocker);
                }
            }
            return result;
        }

        #endregion

        #region Coroutines

        private void ClearAndCollapse(List<Cell> cells)
        {
            StartCoroutine(ClearAndCollapseRoutine(cells));
        }

        private void ClearAndCollapse(List<BoardObject> boardObjects)
        {
            StartCoroutine(ClearAndCollapseRoutine(boardObjects));
        }

        IEnumerator ClearAndCollapseRoutine(List<BoardObject> boardObjects)
        {
            GameController.Instance.DataGame.InputEnable = false;

            List<int> columns = GetColumns(boardObjects);

            yield return new WaitForSeconds(GameController.Instance.DataGame.RoutineTime);

            yield return ClearRoutine(boardObjects);

            ResetTiles();

            yield return new WaitForSeconds(GameController.Instance.DataGame.RoutineTime);

            CollapseColumn(columns);

            yield return new WaitForSeconds(GameController.Instance.DataGame.RoutineTime);

            StartCoroutine(RefillRoutine());
        }

        IEnumerator ClearAndCollapseRoutine(List<Cell> cells)
        {
            GameController.Instance.DataGame.InputEnable = false;

            List<int> columns = GetColumns(cells);

            HighlightMatchesOn(cells);

            yield return new WaitForSeconds(GameController.Instance.DataGame.RoutineTime);

            yield return ClearRoutine(cells);
            ResetTiles();

            yield return new WaitForSeconds(GameController.Instance.DataGame.RoutineTime);

            MakeBombAt(ChooseBomb(cells), DataBoardObjects.ClickedCell.DataPosition.X,
                DataBoardObjects.ClickedCell.DataPosition.Y);
            CollapseColumn(columns);

            yield return new WaitForSeconds(GameController.Instance.DataGame.RoutineTime);

            StartCoroutine(RefillRoutine());
        }

        IEnumerator RefillRoutine()
        {
            FillBoardWithCells();
            yield return new WaitForSeconds(GameController.Instance.DataGame.RoutineTime);

            GameController.Instance.DataGame.InputEnable = true;

            ShowPossibleMatches(DataBoardObjects.AllBoardObjects);
            yield return new WaitForSeconds(GameController.Instance.DataGame.RoutineTime);
        }

        IEnumerator ClearRoutine(List<Cell> objectsToClear)
        {
            if (objectsToClear != null)
            {
                foreach (var obj in objectsToClear)
                {
                    obj.transform.DOPunchScale(new Vector3(1, 1, 0), GameController.Instance.DataGame.ClearTime);
                    yield return new WaitForSeconds(GameController.Instance.DataGame.ClearTime);
                    ClearCellAt(obj.DataPosition.X, obj.DataPosition.Y);
                }
            }
        }

        IEnumerator ClearRoutine(List<BoardObject> objectsToClear)
        {
            if (objectsToClear != null)
            {
                foreach (var obj in objectsToClear)
                {
                    obj.transform.DOPunchScale(new Vector3(1, 1, 0), GameController.Instance.DataGame.ClearTime);
                    yield return new WaitForSeconds(GameController.Instance.DataGame.ClearTime);
                    
                    IClearable clearable = obj as IClearable;
                    if (clearable != null)
                    {
                        clearable.OnClear();
                    }
                   
                }
            }
        }

        #endregion

        private void OnDestroy()
        {
            EventManager.OnClicked -= OnClick;
        }
    }
}