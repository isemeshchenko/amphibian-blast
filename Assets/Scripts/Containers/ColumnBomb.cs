﻿using System.Collections;
using System.Collections.Generic;
using AmphibianSpace;
using UnityEngine;

namespace AmphibianSpace
{
    public class ColumnBomb : Bomb
    {
        private void Awake()
        {
            Setup();
            DataBombType.BombType = BombTypeEnum.Column;
        }
        
        public override List<BoardObject> GetCellsToClear()
        {
            return DataCached.Board.FindColumnMatches(this);
        }
	
    }

}


